const express = require('express');
const axiosConfig = require('../../utils/axiosConfig');
const router = new express.Router();
// const auth = require('../../middleware/auth');
// const firebase = require('../../utils/firebase');

// router.use(auth);

router.get('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    const response = await axiosConfig.get(`/namespaces/mubi/collections/movie/${id}`);
    console.log(response.data);
    res.send(JSON.parse(response.data.data.seats));
  } catch (error) {
    console.log(error);
  }
});

router.post('/', async (req, res) => {
  const { movieId: id, seats, user } = req.body;

  try {
    const response = await axiosConfig.get(`/namespaces/mubi/collections/movie/${id}`);
    const newSeats = JSON.parse(response.data.data.seats);
    seats.forEach(seat => {
      newSeats[seat] = user
    });
    await axiosConfig.patch(`/namespaces/mubi/collections/movie/${id}`, {
      seats: JSON.stringify(newSeats)
    });
    res.send(newSeats);
  } catch (error) {
    console.log(error); 
  }
})

module.exports = router;
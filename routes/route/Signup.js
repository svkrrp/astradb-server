const express = require('express');
const axiosConfig = require('../../utils/axiosConfig');
const router = new express.Router();

router.post('/', async (req, res) => {
  const { email, password } = req.body;
  try {
    const response = await axiosConfig.post(`/keyspaces/mubi/auth`, {
      email, password
    });
    console.log(response.data);
    res.send(response.data);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
})

module.exports = router;
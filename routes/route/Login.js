const express = require('express');
const axiosConfig = require('../../utils/axiosConfig');
const router = new express.Router();

router.post('/', async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await axiosConfig.get(`/keyspaces/mubi/auth/${email}/${password}`)
    console.log("Login", user.data);
    res.send(user.data.data[0]);
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
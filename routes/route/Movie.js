const express = require('express');
const axiosConfig = require('../../utils/axiosConfig');
const router = new express.Router();

router.post('/addmovie', async (req, res) => {
  const { movie } = req.body;
  const seats = {};
  Array(84).fill(0).forEach((v, i) => {
    return seats[i] = v;
  })
  try {
    const newMovie = {
      ...movie,
      seats: JSON.stringify(seats)
    }
    const response = await axiosConfig.post('/namespaces/mubi/collections/movie', newMovie);
    console.log("addmovie", response.data);
    res.send(response.data.documentId);
  } catch (error) {
    res.send(error);
  }
});

router.get('/movies', async (req, res) => {
  const movies = [];
  try {
    const response = await axiosConfig.get('/namespaces/mubi/collections/movie?page-size=20');
    Object.keys(response.data.data).forEach(movieId => {
      movies.push({
        id: movieId,
        ...response.data.data[movieId]
      })
    });
    res.send(movies);
  } catch (error) {
    res.send(error);
  }
});

router.post('/delete/:id', async (req, res) => {
  const { id } = req.params;
  try {
    await axiosConfig.delete(`/namespaces/mubi/collections/movie/${id}`);
    res.send('successfully deleted');
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
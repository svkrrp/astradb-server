const express = require('express');
const router = new express.Router();

const SignUp = require('./route/Signup');
const Login = require('./route/Login');
const LogOut = require('./route/LogOut');
const Movie = require('./route/Movie');
const Seats = require('./route/Seats');

router.use('/movie', Movie);
router.use('/signup', SignUp);
router.use('/login', Login);
router.use('/logout', LogOut);
router.use('/seats', Seats);

module.exports = router;
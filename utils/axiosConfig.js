const axios = require("axios");

const clusterId = "a375000e-2b41-4692-8cab-aeeab5798078";
const region = "ap-southeast-1";

const headers = {
  "X-Cassandra-Token":
    "AstraCS:CPYJfJksqtpPABJQaGupkPMF:e63969a1aa8b6e7e691e85a1987bc5a5ff30e2505059b83fa922bc1572eb2e82",
  "Content-Type": "application/json",
};

const axiosConfig = axios.create({
  baseURL: `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/`,
  headers,
});

module.exports = axiosConfig;

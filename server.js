const express = require('express');
const app = express();
const appRoute = require('./routes/app');
const cors = require('cors');

const PORT = process.env.PORT || 4200;

app.use(cors());
app.use(express.json()); 
app.use(express.urlencoded({ extended: true })); 
app.use(appRoute);

app.get('/health', (req, res) => {
  res.send('success');
})

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

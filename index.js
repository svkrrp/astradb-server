const express = require("express");
const app = express();
const axios = require("axios");

const axiosConfig = require("./axiosConfig.js");

const PORT = process.env.PORT || 5000;

const clusterId = "a375000e-2b41-4692-8cab-aeeab5798078";
const region = "ap-southeast-1";

const headers = {
  "X-Cassandra-Token":
    "AstraCS:CPYJfJksqtpPABJQaGupkPMF:e63969a1aa8b6e7e691e85a1987bc5a5ff30e2505059b83fa922bc1572eb2e82",
  "Content-Type": "application/json",
};

app.get("/write-db", async (req, res) => {
  try {
    const response = await axios.post(
      `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/keyspaces/mubi/users`,
      {
        firstname: "shivam",
        lastname: "kumar",
        favorite_color: "red",
      },
      {
        headers,
      }
    );
    console.log(response);
  } catch (err) {
    console.log("error", err);
  }
});

app.get("/update-row", async (req, res) => {
  try {
    const response = await axios.put(
      `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/keyspaces/mubi/users/shivam/kumar`,
      {
        favorite_color: "red",
      },
      {
        headers,
      }
    );
    console.log(response);
  } catch (err) {
    console.log("error", err);
  }
});

app.get("/delete-row", async (req, res) => {
  try {
    const response = await axios.delete(
      `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/keyspaces/mubi/users/shivam`,
      {
        headers,
      }
    );
    console.log(response);
  } catch (err) {
    console.log("error", err);
  }
});

app.get("/read-db", async (req, res) => {
  try {
    // const response = await axios.get(
    //   `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/keyspaces/mubi/users/shivam`,
    //   {
    //     headers,
    //   }
    // );
    const response = await axiosConfig.get("/mubi/users/shivam");
    console.log(response);
  } catch (err) {
    console.log("error", err);
  }
});

// app.get("/create-table", async (req, res) => {
//   try {
//     const response = await axios.post(
//       `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/schemas/keyspaces/mubi/tables`,
//       {
//         name: "users",
//         columnDefinitions: [
//           {
//             name: "firstname",
//             typeDefinition: "text",
//           },
//           {
//             name: "lastname",
//             typeDefinition: "text",
//           },
//           {
//             name: "favorite_color",
//             typeDefinition: "text",
//           },
//         ],
//         primaryKey: {
//           partitionKey: ["firstname"],
//           clusteringKey: ["lastname"],
//         },
//       },
//       {
//         headers,
//       }
//     );
//     console.log(response);
//   } catch (err) {
//     console.log("error", err);
//   }
// });

// app.get("/create-table", async (req, res) => {
//   try {
//     const response = await axios.post(
//       `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/schemas/keyspaces/mubi/tables`,
//       {
//         name: "auth",
//         columnDefinitions: [
//           {
//             name: "email",
//             typeDefinition: "text",
//           },
//           {
//             name: "password",
//             typeDefinition: "text",
//           },
//           {
//             name: "name",
//             typeDefinition: "text",
//           },
//         ],
//         primaryKey: {
//           partitionKey: ["email"],
//           clusteringKey: ["password"],
//         },
//       },
//       {
//         headers,
//       }
//     );
//     console.log(response);
//   } catch (err) {
//     console.log("error", err);
//   }
// });

app.get("/create-table", async (req, res) => {
  try {
    const response = await axios.post(
      `https://${clusterId}-${region}.apps.astra.datastax.com/api/rest/v2/schemas/keyspaces/mubi/tables`,
      {
        name: "movies",
        columnDefinitions: [
          {
            name: "imageUrl",
            typeDefinition: "text",
          },
          {
            name: "name",
            typeDefinition: "text",
          },
          {
            name: "time",
            typeDefinition: "text",
          },
          {
            name: "seats",
            typeDefinition: "text"
          }
        ],
        primaryKey: {
          partitionKey: ["name"],
          clusteringKey: ["time"],
        },
      },
      {
        headers,
      }
    );
    console.log(response);
  } catch (err) {
    console.log("error", err);
  }
});

app.listen(PORT, () => {
  console.log("listening on PORT " + PORT);
});

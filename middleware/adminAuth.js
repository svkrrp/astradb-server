const firebase = require('../utils/firebase');

const auth = (req, res, next) => {
  const user = firebase.auth().currentUser;
  if(user) {
    console.log("in middleware " + user.uid);
    next();
  } else {
    res.send("user not logged in");
  }
}

module.exports = auth;